{{-- s02 Activity solution --}}
@extends('layouts.app')

@section('content')
    @if(count($posts) >  0)
        <div class="text-center">
            <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" class="h-25 w-25 mb-4" />
            <h1>Featured Posts:</h1>
        </div>
		@foreach($posts as $post)
			<div class="card text-center w-75 mx-auto mb-2">
				<div class="card-body">
					<h4 class="card-title mb-3">
						<a href="/posts/{{$post->id}}">{{$post->title}}</a>
					</h4>
					<h6 class="card-text mb-3">
						Author: {{$post->user->name}}
					</h6>
				</div>
            </div>

			@if(Auth::user())
				{{-- Check if the authenticated user is the author of the blog post --}}
				@if(Auth::user()->id == $post->user_id)

				@endif
			@endif
		@endforeach
	@else
		<div>
			<h2>There are no posts to show.</h2>
			<a href="/posts/create" class="btn btn-info">Create post</a>
		</div>
	@endif











@endsection()


