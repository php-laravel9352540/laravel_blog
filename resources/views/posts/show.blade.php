@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title text-muted">{{$post->title}}</h2>
            <p class="card-subtitle">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>
            <p class="card-text">Likes: {{count($post->likes)}} | Comments: {{count($post->comments)}}</p>

            @if(Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf
                    @if($post->likes->contains("user_id",   Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                </form>
            @endif

            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Comment</button>

            @if(Auth::id())
                <form method="POST" action="/posts/{{$post->id}}/comment">
                    @method('PUT')
                    @csrf
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Leave a comment</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <textarea class="form-control" id="content" name="content" rows="3"></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                            </div>
                        </div>
                    </div>
                </form>
            @endif

            <div class="mt-3">
                <a href="/posts" class="card-link">View all posts</a>
            </div>

        </div>
    </div>


    <div class="mt-4">
        <h4>Comments:</h4>
        <ul class="list-group">
        @if(count($post->comments) > 0)
            @foreach($post->comments as $comment)
                <li class="list-group-item">
                    <h4 class="text-center mt-3">{{ $comment->content }}</h4>
                    <h5 class="text-end"><span class="text-muted">Posted by:</span> {{ $comment->user->name }}</h5>
                    <p class="text-end text-muted">posted on: {{ $comment->created_at }}</p>
                </li>
            @endforeach
            @else
                <div class="text-center mt-5">
                    <h2>There are no comments to show.</h2>
                </div>
        @endif
        </ul>
    </div>

@endsection
